part of './pages.dart';

abstract class Routes{
  static const AUTH = '/auth';
  static const HOME = '/home';
  static const INITIAL = '/home';
}