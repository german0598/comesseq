import 'package:flutter/material.dart';

class AppColors {
  static final primary  = const Color(0xffBA181B);
  static final labels   = const Color(0xff0B090A);
  static final parrafos = const Color(0xff161A1D);
  static final blanco   = const Color(0xffFFFFFF);
}
