// import 'package:flutter/material.dart';

import 'package:comesseq/app/data/models/loguedUser.dart';
import 'package:comesseq/app/data/repositories/storage_repository.dart';
import 'package:comesseq/app/modules/auth/auth_repository.dart';
import 'package:comesseq/app/routes/pages.dart';
import 'package:get/get.dart';

class AuthController extends GetxController {
  final AuthRepository _repository = Get.find<AuthRepository>();
  final StorageRepository _storageRepository = Get.find<StorageRepository>();

  String _username      = '';
  String _password      = '';
  RxBool _showPassword  = false.obs;
  RxBool _loading       = false.obs;
  Map<String, dynamic> _usuario;

  get username    => _username;
  get password    => _password;
  get loading     => _loading;
  get shoPassword => _showPassword;
  get usuario async {
    if (usuario == null) {
      UserAuthenticated user = await _storageRepository.getAuthUser();
      _usuario = user.toJson();
    }
    return _usuario;
  }

  void onUsernameChanged(String text) {
    _username = text;
  }

  void onPasswordChanged(String text) {
    _password = text;
  }

  void onShowPassword(bool mostrar) {
    _showPassword.value = mostrar;
  }

  void onLoading( bool loading ) {
    _loading.value = loading;
  }

  void setUsuario( Map<String, dynamic> usuario ) {
    print('[USUARIO GUARDAR] $usuario');
    _usuario = usuario;
    _storageRepository.saveUserAuth( usuario: usuario );
  }

  void resetUsuario() {
    _usuario = {};
  }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    print('AuthController renderizado - Listo.');
  }

  @override
  void onClose() {
    super.onClose();
    print('AuthController eliminandose de memoria.');
  }

   Future<void> submit() async {
    try {
      // RequestToken requestToken = await _repository.newRequestToken();
      final UserAuthenticated userAuthenticated = await _repository.authWithLogin(
        username: _username,
        password: _password,
      );
      setUsuario( userAuthenticated.toJson() );
      Get.offNamed( Routes.HOME );
    } catch (e) {
      print("=================== [ERROR] ===================");
      print(e);
      // String message = "";
      // if (e is DioError) {
      //   if (e.response != null) {
      //     message = e.response.statusMessage;
      //   } else {
      //     message = e.message;
      //   }
      // }
      // Get.dialog(
      //   AlertDialog(
      //     title: Text("ERROR"),
      //     content: Text(message),
      //     actions: [
      //       FlatButton(
      //         child: Text("OK"),
      //         onPressed: () {
      //           Get.back();
      //         },
      //       )
      //     ],
      //   ),
      // );
    }
  }
}