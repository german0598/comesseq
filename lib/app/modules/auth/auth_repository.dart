
import 'package:comesseq/app/data/models/loguedUser.dart';
import 'package:comesseq/app/data/providers/auth_api.dart';
import 'package:meta/meta.dart' show required;
import 'package:get/get.dart';

class AuthRepository {

  final AuthProvider _authApi = Get.find<AuthProvider>();

  Future<UserAuthenticated> authWithLogin({
    @required String username,
    @required String password,
  }) => _authApi.validateWithLogin( username: username, password: password );

}