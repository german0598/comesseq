// import 'package:dio/dio.dart';
import 'package:comesseq/app/data/models/loguedUser.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:comesseq/app/core/values/constants.dart';
import 'package:get/get.dart';
// import 'package:meta/meta.dart' show required;
import 'dart:convert';
class StorageProvider {

  final FlutterSecureStorage _storage = Get.find<FlutterSecureStorage>();

  void saveUserAuth( Map<String, dynamic> usuario ) {
    final mapUserString = jsonEncode( usuario );
    print(' ==== USUARIO A GUARDAR ==== ');
    print( mapUserString );
    _storage.write(key: Constants.KEY_USER_STORAGE, value: mapUserString);
  }

  Future<UserAuthenticated> getUserAuth() async {
    final userString = await _storage.read(key: Constants.KEY_USER_STORAGE);
    final userDecode = jsonDecode( userString );
    final UserAuthenticated user = UserAuthenticated(nombre: userDecode.nombre, cedula: userDecode.cedula, token: null);
    return user;
  }

  void saveItem({ dynamic item, String key }) {
    String stringItem = '';
    if ( item is String ) {
      stringItem = item;
    } else if( item is Map ) {
      stringItem = jsonEncode( stringItem );
    }

    _storage.write(key: key, value: stringItem);
  }

}
