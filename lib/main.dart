import 'package:comesseq/app/modules/auth/auth_binding.dart';
import 'package:comesseq/app/modules/auth/auth_page.dart';
import 'package:comesseq/app/routes/pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:comesseq/app/core/utils/dependency_injection.dart';

void main() {
  DependencyInjection.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Comesseq',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity
      ),
      home: AuthPage(),
      initialBinding: AuthBinding(),
      getPages: AppPages.pages,
    );
  }
}
